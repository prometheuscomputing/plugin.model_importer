# This may be used for future tests
project "DuplicateNames", :version => "0.0.0", :original_filename => "duplicate_names.dsl.rb", :generated_by => "mfaughn@prometheuscomputing.com", :generated_at => "2017-10-27", :additional_info => {:selected_model=>{:name=>"Application", :id=>"myProjectID"}, :id => "myApplicationID" do
  model "Application", :imports => ["Foo"], :id => "myApplicationID"
  package "Foo", :id => "FooPackageID1" do
    association :properties => ["Foo::Foo::foo_property", "Foo::Foo::foo_property"], :id => "foo_foo_AssociationID1"
    association :properties => ["Foo::Foo::foo_property", "Foo::Foo::bar_property"], :id => "foo_bar_AssociationID1"
    association :properties => ["Foo::Foo::foo_property", "Foo::Foo::bar_property"], :id => "foo_bar_AssociationID1"
    klass "Foo", :id => "FooClassID1" do
      property "foo_property", :type => "UML::String", :id => "foo_propertyID1"
      property "foo_property", :type => "UML::String", :id => "foo_propertyID2"
      property "bar_property", :type => "UML::String", :id => "bar_propertyID1"
    end
    klass "Foo", :id => "FooClassID2" do
    end
    enumeration "Foo", :id => "FooEnumerationID" do
      property "value", :type => "UML::String"
      literal "Literal", :id => "literalID1"
      literal "Literal", :id => "literalID2"
    end
  end
  package "Foo", :id => "FooPackageID1"
end
