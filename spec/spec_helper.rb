require 'rspec'
require 'rainbow'
require_relative '../lib/model_importer/load_path_manager'
LoadpathManager.amend_load_path
# $:.unshift relative('../../magicdraw_plugin_runner/lib/')
require 'magicdraw_plugin_runner'

# def setup_file_path_var(path, var)
#   code_dir       = File.expand_path('~/Prometheus/Generated_Code/')
#   @filename_base = path.split('/').last.slice(/.+(?=\.rb)/)
#   puts Rainbow(@filename_base.inspect).yellow
#   var            = File.join(code_dir, @filename_base + '.rb')
#   # Remove it if it exists
#   FileUtils.rm(var) if File.exist?(var)
# end