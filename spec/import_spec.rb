require 'spec_helper'
require 'fileutils'
require 'uml_metamodel'

describe 'Ruby Model Importer' do
  before :all do
    # Get the path to the test models expressed in the DSL
    @input_dsl_file  = File.expand_path('~/projects/uml_metamodel/test_data/CarExampleApplication.rb')
    @output_dsl_file = File.expand_path('../test_output.rb', __FILE__)
    FileUtils.rm(@output_dsl_file) if File.exist?(@output_dsl_file)
    
    test_file      = File.expand_path('../test_raw.mdzip', __FILE__)
    @copy_filename = 'test.mdzip'
    copy           = File.expand_path("../#{@copy_filename}", __FILE__)
    FileUtils.cp(test_file, copy)
    importer_runner_path = File.expand_path('../../lib/model_importer/model_importer_runner.rb', __FILE__)
    test_pkg    = 'Data'
    retest_pkg  = 'Application'
    runner_args = [copy,
                   test_pkg,
                   "--input_dsl_file=#{@input_dsl_file}",
                   "--output_dsl_file=#{@output_dsl_file}",
                   "--generation_package=#{retest_pkg}"]
            
    @message = "Import Action Result:\n"
    # Run the plugin on the DSL file
    @message << MagicdrawPluginRunner.run(importer_runner_path, runner_args)
    puts @message
  
    @starting_project = UmlMetamodel.from_dsl(File.read(@input_dsl_file))
    expect(File.exist?(@output_dsl_file)).to be true
    @result_project = UmlMetamodel.from_dsl(File.read(@output_dsl_file))
    # uses Kaleidoscope if it is installed, otherwise uses default diff tool
    @diff_cmd = "if [ -x \"$(command -v ksdiff)\" ]; then ksdiff #{@input_dsl_file} #{@output_dsl_file}; else diff #{@input_dsl_file} #{@output_dsl_file}; fi"
  end
  
  it "should not produce any errors" do
    expect(@message).not_to include('Error')
  end

  it "should not produce any warnings" do
    expect(@message).not_to include('Succeeded with warnings:')
  end

  it "should generate a top-level project wrapper" do
    expect(@result_project).to be_a(UmlMetamodel::Project)
  end
  
  it "should generate a top-level project wrapper identical to the starting project" do
    expect(@result_project).to be_a(UmlMetamodel::Project)
    expect(@result_project.name).to eq(@starting_project.name) 
    expect(@result_project.version).to eq(@starting_project.version) 
    expect(@result_project.original_filename).to eq(@copy_filename) 
  end

  # 
  # README
  # This test fails if you just check to see if all of the resulting project contents are exactly the same as they were in the starting project.  You should look at the diff and be sure that the only reason that it is failing is that the version number from the original input file's Project element is getting applied to the output file's Application model.  This should be the only difference.  The reason this is happening is in order to ensure that the version information is carried forward.  When the model is imported the version is put on the Application.  When modelGen exports it again it is copied to the project also (maybe it shouldn't be).
  # Note that differences in the attribution of the starting and result Project elements are not tested here (and for good reason.)
  it "the contents of the resulting project should be the same as the contents of the starting project" do
    rc = @result_project.contents
    sc = @starting_project.contents
    same = rc.all? { |pkg| pkg.name == 'Application' || pkg.matches?(sc[rc.index(pkg)], true) }
    # system @diff_cmd unless same # You may want to uncomment if running this test by itself
    expect(same).to be_truthy
    sa = sc.find { |pkg| pkg.name == 'Application' }
    ra = rc.find { |pkg| pkg.name == 'Application' }
    expect(sa.name).to eq(ra.name)
    expect(sa.id).to eq(ra.id)
    expect(sa.imports.collect(&:name)).to eq(ra.imports.collect(&:name))
    same = ra.contents.all? { |e| e.matches?(sa.contents[ra.index(e)], true) }
    expect(same).to be_truthy
  end
  
  it "should show the developer a diff of the starting dsl against the resulting, regenerated dsl" do
    system @diff_cmd
    puts Rainbow("HEY TESTER!  READ THIS!!").red.bright
    puts "You should read and understand these tests and then use your judegement by looking at a diff to see if things are alright or not."
  end
  
  
end

# UmmImporter.import_models('uml_metamodel/../../test_data/CarExample.rb'.find_on_load_path)