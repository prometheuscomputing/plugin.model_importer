# Modeler plugin for MagicDraw: builds UML models
# Copyright (C) 2017  Prometheus Computing LLC
require 'rainbow'
require 'Foundation/load_path_management'
require 'uml_metamodel'
# load 'uml_metamodel/elements/applied_stereotype.rb'
# load 'uml_metamodel/elements/stereotype.rb'
# load 'uml_metamodel/elements/property.rb'
# load 'uml_metamodel/elements/association.rb'
# load 'uml_metamodel/dsl.rb'
# load 'uml_metamodel/find.rb'
# load 'magicdraw_extensions.rb'
require 'umm_adapter'
require 'magicdraw_extensions/file_dialogs'

# This is used by MagicDraw 18.x, but deprecated in 19.x
java_import 'com.nomagic.magicdraw.uml.symbols.shapes.html.HTMLHelper'

require 'fileutils'
include FileUtils::Verbose # or FileUtils::NoWrite

module UmmImporter
  def self.import_models(selection)
    dsl_path = $import_test_input || MagicDraw.get_dsl_path
    STDERR.puts "THE DSL PATH is #{dsl_path}"
    $import_test_input = nil
    return $MESSAGE = 'CANCELLED: No file specified' unless dsl_path && !dsl_path.empty?
    return $MESSAGE = 'CANCELLED: Could not find file specified' unless File.exists?(dsl_path)
    root_element = UmlMetamodel.from_dsl(File.read(dsl_path))
    dsl_path = nil # must do in order to safely clear things
    messages = ModelBuilder.new.build(root_element, selection)
    $MESSAGE = messages.uniq.join("\n")
  end
end

module MagicDraw
  def self.get_dsl_path
    options = { 
      :setFileSelectionMode => JFileChooser::FILES_ONLY,
      :setFileFilter        => RubyFileFilter.new('model files', '.rb')
    }
    test_file_dir = File.expand_path("~/projects/uml_metamodel/test_data")
    # if File.exist?(File.join(test_file_dir, "CarExampleApplication.rb"))
    #   return File.join(test_file_dir, "CarExampleApplication.rb")
    # end
    $File_Chooser = JFileChooser.new(test_file_dir) if Dir.exist?(test_file_dir)
    show_open_dialog(options) # FIXME this is from magicdraw_extensions.  It makes me cringe that the method is not namespaced.
  end
end

########################################################################################################################

#  Note: most of the methods that require names have an internal to_s, so that Symbols can be used instead.
class ModelBuilder
  attr_reader :elements, :messages
  # These map to the UmlMetamodel types
  attr_reader :applied_stereotypes, :applied_tags, :associations, :classes, :datatypes, :element_instances, :enumerations, :literals, :interfaces, :models, :packages, :primitives, :profiles, :projects, :properties, :stereotypes, :tags
  
  def initialize
    @elements = {}
    # not all of these are currently used but it might be useful to have them collected like this at some future point.
    @applied_stereotypes = []
    @applied_tags        = []
    @associations        = []
    @classes             = []
    @datatypes           = []
    @enumerations        = []
    @literals            = []
    @interfaces          = []
    @models              = []
    @packages            = []
    @primitives          = []
    @profiles            = []
    @projects            = []
    @properties          = []
    @stereotypes         = []
    @tags                = []
    
    @messages             = []
  end
  
  def build(root_element, selected_container)
    STDERR.puts "THE root_element is a #{root_element.class} named #{root_element.name}"
    STDERR.puts "THE selected_container is #{selected_container.class} named #{selected_container.getName}"
    @root_element = root_element
    register_uml_metamodel_adapter_profile
    register_element(root_element)
    elements.each { |e, data| data[:md_element] ||= make(e) }
    set_ownerships(root_element, selected_container)
    connect_associations
    set_stereotype_metaclasses
    set_ownerships(@adapter_profile, MagicDraw.root_model)
    set_attribute_types
    set_inheritance
    set_interface_implementation
    apply_stereotypes
    set_imports
    set_documentation
    apply_uml_metamodel_adapter_stereotypes(root_element)
    @messages << 'Success.  No errors or warnings.' if @messages.empty?
    # report
    messages
  end
  
  # For debugging
  def report
    puts '_'*30
    puts "All elements that respond to getQualifiedName() that are not in UML Standard Profile"
    puts '_'*30
    # MagicDraw.root_model.all_elements(["UML Standard Profile"]).each {|e| puts "(#{e.inspect})\n  @QualifiedName => '#{e.getQualifiedName}'\n  @ID => #{e.getID}"}
    MagicDraw.root_model.all_elements(["UML Standard Profile"]).each { |e|
      next unless (e.is_a?(Java::ComNomagicUml2ExtMagicdrawClassesMdkernelImpl::AssociationImpl) || e.is_a?(Java::ComNomagicUml2ExtMagicdrawClassesMdassociationclassesImpl::AssociationClassImpl))
      se = ModelHelper.getSupplierElement(e)
      ce = ModelHelper.getClientElement(e)
      puts "#{e.inspect}"
      puts "@Supplier => #{se ? se.inspect : 'nil'}"
      puts "@Client => #{ce ? ce.inspect : 'nil'}"
    }
    
    puts '_'*30
  end
  
  def register_element(umm_element)
    md_element = MagicDraw.project.getElementByID(umm_element.id)
    if md_element.nil? && umm_element.is_a?(UmlMetamodel::Class) && umm_element.association_class_for
      # Then the md_element should be an AssociationClass
      assoc_class_id = "#{umm_element.id}#{UmmAdapter::ID_CONCAT_CHAR}#{umm_element.association_class_for.id}"
      md_element = MagicDraw.project.getElementByID(assoc_class_id)
    end
    elements[umm_element] = {:id => umm_element.id, :md_element => md_element }
    element_class = umm_element.class
    case
    when element_class == UmlMetamodel::AppliedStereotype
      applied_stereotypes << umm_element # probably unnecessary
    when element_class == UmlMetamodel::AppliedTag
      applied_tags << umm_element # probably unnecessary
    when element_class == UmlMetamodel::Association
      # Do not register associations for assocation classes because MD uses the OMG UML metamodel and our metamodel doesn't have association classes
      associations << umm_element unless umm_element.association_class
    when element_class == UmlMetamodel::Class
      classes << umm_element
      # if it is an assocaition class then it will need to be processed both as a class and as an association
      associations << umm_element if umm_element.association_class?
      umm_element.properties.each { |prop| register_element(prop) }
    when element_class == UmlMetamodel::Datatype
      datatypes << umm_element
      umm_element.properties.each { |prop| register_element(prop) }
    when element_class == UmlMetamodel::AppliedStereotype || element_class == UmlMetamodel::AppliedTag
      element_instances << umm_element # Note: we don't actually do anything with element_instances...
    when element_class == UmlMetamodel::Enumeration
      enumerations << umm_element
      umm_element.literals.each { |lit| register_element(lit) }
      # should we be ignoring the value property?
      umm_element.properties.each { |prop| register_element(prop) unless prop.name == 'value'}
    when element_class == UmlMetamodel::EnumerationLiteral
      literals << umm_element
    when element_class == UmlMetamodel::Interface
      interfaces << umm_element
      umm_element.properties.each { |prop| register_element(prop) }
    when element_class == UmlMetamodel::Model
      models << umm_element
      umm_element.contents.each { |e| register_element(e) }
    when element_class == UmlMetamodel::Profile
      profiles << umm_element
      umm_element.contents.each { |e| register_element(e) }
    when element_class == UmlMetamodel::Package
      packages << umm_element
      umm_element.contents.each { |e| register_element(e) }
    when element_class == UmlMetamodel::Primitive
      primitives << umm_element
      umm_element.properties.each { |prop| register_element(prop) }
    when element_class == UmlMetamodel::Project
      elements[umm_element][:md_element => MagicDraw.project] unless elements[umm_element][:md_element]
      projects << umm_element
      umm_element.contents.each { |e| register_element(e) }
    when element_class == UmlMetamodel::Property
      properties << umm_element
    when element_class == UmlMetamodel::Stereotype
      stereotypes << umm_element
      umm_element.tags.each { |tag| register_element(tag) }
    when element_class == UmlMetamodel::Tag
      tags << umm_element
    else
      raise "I don't know how to register a #{umm_element.inspect}"
    end
  end
  
  def register_uml_metamodel_adapter_profile
    @adapter_profile = UmmAdapter.adapter_profile
    register_element(@adapter_profile)
  end
  
  # assumes root_element is a UmlMetamodel::Project
  # TODO refactor for DRYness
  def apply_uml_metamodel_adapter_stereotypes(root_element)
    info = root_element.additional_info || {}
    selected_model_id = info[:selected_model] && info[:selected_model][:id]
    if selected_model_id
      selected_umm_model = elements.keys.find { |umm_el| umm_el.id == selected_model_id }
      selected_md_model  = elements[selected_umm_model][:md_element] if elements[selected_umm_model]
    end
    selected_md_model ||= MagicDraw.root_model
    md_stereotypes = @adapter_profile.stereotypes.collect { |e| elements[e][:md_element] }
    md_stereotypes.each { |mdst| selected_md_model.apply_stereotype(mdst) }
    if info[:selected_model]
      umm_generation_options = @adapter_profile.stereotypes.find { |st| st.name == 'generation_options' }
      md_generation_options  = elements[umm_generation_options][:md_element]
      prn  = info[:selected_model][:pluralize_role_names]
      unless prn.nil?
        umm_tag = umm_generation_options.tags.find { |t| t.name == 'pluralize_role_names' }
        md_tag  = elements[umm_tag][:md_element]
        selected_md_model.apply_tag_with_value(md_generation_options, md_tag, prn)
      end
      scrn = info[:selected_model][:snakecase_role_names]
      unless scrn.nil?
        umm_tag = umm_generation_options.tags.find { |t| t.name == 'snakecase_role_names' }
        md_tag  = elements[umm_tag][:md_element]
        selected_md_model.apply_tag_with_value(md_generation_options, md_tag, scrn)
      end
    end
    
    umm_metainformation = @adapter_profile.stereotypes.find { |st| st.name == 'metainformation' }
    md_metainformation  = elements[umm_metainformation][:md_element]
    if root_element.name
      umm_tag = umm_metainformation.tags.find { |t| t.name == 'project_name' }
      md_tag  = elements[umm_tag][:md_element]
      selected_md_model.apply_tag_with_value(md_metainformation, md_tag, root_element.name)
    end
    if root_element.version
      umm_tag = umm_metainformation.tags.find { |t| t.name == 'version' }
      md_tag  = elements[umm_tag][:md_element]
      selected_md_model.apply_tag_with_value(md_metainformation, md_tag, root_element.version)
    end

  end

  def set_stereotype_metaclasses
    stereotypes.each do |umm_stereotype|
      md_stereotype = elements[umm_stereotype][:md_element]
      base_classes  = umm_stereotype.metaclasses.collect { |metaclass| get_uml_metaclass(metaclass) }
      StereotypesHelper.setBaseClasses(md_stereotype, base_classes)
    end
  end

  def set_attribute_types
    properties.each do |umm_prop|
      next unless umm_prop.type
      # next if umm_prop.association
      md_prop  = elements[umm_prop][:md_element]
      unless md_prop
        STDERR.puts Rainbow(umm_prop.qualified_name).cyan
        if umm_prop.association
          STDERR.puts Rainbow(umm_prop.association.to_dsl).cyan
        end
        STDERR.puts
        @messages << "ERROR: Failed to find MagicDraw property corresponding to #{umm_prop.qualified_name} while setting attribute types."
        next
      end
      umm_type = find_umm_element_by_name(umm_prop.type.qualified_name, true)
      if umm_type
        md_prop.setType(elements[umm_type][:md_element])
      else
        prim = get_uml_primitive(umm_prop.type)
        md_prop.setType(prim) if prim # TODO error should be generated in #get_uml_primitive if prim.nil?
      end
    end
  end
  
  def get_uml_primitive(type)
    str = type.qualified_name
    case
    when str == 'UML::String'
      MagicDraw.uml_primitives[:string]
    when str == 'UML::Integer' 
      MagicDraw.uml_primitives[:integer]
    when str == 'UML::Boolean' 
      MagicDraw.uml_primitives[:boolean]
    when str == 'UML::Real' 
      MagicDraw.uml_primitives[:real]
    when str == 'UML::UnlimitedNatural'
      MagicDraw.uml_primitives[:unlimitednatural]
    when str == 'UML::Float' 
      MagicDraw.md_profile_datatypes[:float]
    when str == 'UML::Null'
      MagicDraw.md_profile_datatypes[:void]
    when str == 'UML::ByteString'
      MagicDraw.md_profile_datatypes[:byte]
    when str == 'UML::Char'
      MagicDraw.md_profile_datatypes[:char]
    when str == 'UML::Date'
      MagicDraw.md_profile_datatypes[:date]
    when str == 'UML::Datetime' # This one is kind of iffy
      MagicDraw.md_profile_datatypes[:date]
    when str == 'UML::Time'
      messages << "ERROR: no type mapping for type #{str.qualified_name}"
      nil
    when str == 'UML::Uri'
      messages << "ERROR: no type mapping for type #{str.qualified_name}"
      nil
    when str == 'UML::RegularExpression'
      messages << "ERROR: no type mapping for type #{str.qualified_name}"
      nil
    else
      messages << "ERROR: no type mapping for type #{str.qualified_name}"
      nil
    end
    # There are no apparent types in the UML package from the UmlMetamodel that would map to these...
    # MagicDraw.md_profile_datatypes[:short]
    # MagicDraw.md_profile_datatypes[:long]
    # MagicDraw.md_profile_datatypes[:double]
    # MagicDraw.md_profile_datatypes[:structuredexpression]
    # MagicDraw.md_profile_datatypes[:xml]
  end
  
  def get_uml_metaclass(type)
    str = type.is_a?(Class) ? type.name : type
    md_class = MagicDraw.uml_classes[str.downcase.to_sym]
    raise "No type for #{type.inspect}" unless md_class
    md_class
  end

  def connect_associations
    assoc_classes, assocs = associations.partition { |a| a.is_a?(UmlMetamodel::Class) }
    (assoc_classes + assocs).each do |umm_assoc|
      md_assoc = elements[umm_assoc][:md_element]
      
      # In this case umm_assoc is not really an assoc but a class.  We actually need the UmlMetamodel::Association so we get it from the UmlMetamodel::Class.  The reason we have to do it this way is that the registry isn't going to have an md_element for the actual UmlMetamodel::Association because our metamodel is different from the OMG UML metamodel.  This could be rearranged but ya know...
      umm_assoc = umm_assoc.association_class_for if umm_assoc.is_a?(UmlMetamodel::Class)
      
      # We are only supporting binary associations.
      messages << "WARNING: #{umm_assoc.class}#{umm_assoc.qualified_name} had more than two properties!" if umm_assoc.properties.count > 2
      umm_member_end_1  = umm_assoc.properties.first
      umm_member_end_2  = umm_assoc.properties.last

      umm_supplier = umm_member_end_1.owner
      umm_client   = umm_member_end_2.owner
      md_supplier  = elements[umm_supplier][:md_element]
      md_client    = elements[umm_client][:md_element]
      
      # Based on the implication from MD forum response by MD personel...
      # This adopts the arbitrary convention that
      #    * first-member-end is first property in the list and is owned by the supplier element
      #    * second-member-end is last (presumably second) property in the list and is owned by the client element
      begin
        # Honestly, I have no clue why this would work or how it relates the the UML spec.  The MD OpenAPI is such garbage.  The MagicDraw people refuse to explain how this has anything to do with the UML spec.
        ModelHelper.setSupplierElement(md_assoc, md_supplier)
        ModelHelper.setClientElement(  md_assoc, md_client)
        md_member_end_1 = ModelHelper.getFirstMemberEnd( md_assoc)
        md_member_end_2 = ModelHelper.getSecondMemberEnd(md_assoc)
        # Set the names and multiplicity of the properties
        configure_property(md_member_end_1, umm_member_end_1)
        configure_property(md_member_end_2, umm_member_end_2)
        # Add these new properties to the plugin's elements registry
        elements[umm_member_end_1][:md_element] = md_member_end_1
        elements[umm_member_end_2][:md_element] = md_member_end_2
        ModelHelper.setNavigable(md_member_end_1, umm_member_end_1.is_navigable);
        ModelHelper.setNavigable(md_member_end_2, umm_member_end_2.is_navigable);
      rescue
        puts Rainbow(umm_assoc.pretty_inspect).magenta
        puts Rainbow(md_assoc.pretty_inspect).red
        raise
      end
    end
  end
  
  def set_visibility(md_property, umm_property)
    case
    when umm_property.visibility == :private
      md_property.setVisibility(MagicDraw::PRIVATE)
    when umm_property.visibility == :protected
      md_property.setVisibility(MagicDraw::PROTECTED)
    else
      md_property.setVisibility(MagicDraw::PUBLIC)
    end
  end
  
  def set_inheritance
    elements.each do |umm_element, info|
      next unless umm_element.is_a?(UmlMetamodel::Classifier)
      this_md_element = elements[umm_element][:md_element]
      umm_element.children.each { |umm_child| generalize(elements[umm_child][:md_element], this_md_element) }
      if umm_element.is_a?(UmlMetamodel::Primitive)
        umm_element.parents.each do |umm_parent|
          if UmlMetamodel::PRIMITIVES.values.include?(umm_parent)
            md_parent = get_md_primitive(umm_parent)
            generalize(this_md_element, md_parent)
          end
        end
      end
    end
  end
  
  def get_md_primitive(umm_element)
    md_element = (UmmAdapter.umm_to_md_primitive_mapping.find { |k, _| k.qualified_name == umm_element.qualified_name } || []).last
  end

  def set_interface_implementation
    elements.each do |umm_element, info|
      next unless umm_element.is_a?(UmlMetamodel::Interface)
      md_iface       = elements[umm_element][:md_element]
      md_iface_owner = elements[umm_element][:md_owner]
      # Arbitrarily deciding that whatever package the interface is in will also own the realization element.
      umm_element.implementors.each { |umm_impl| realize(md_iface_owner, elements[umm_impl][:md_element], md_iface) }
    end
  end
  
  def set_imports
    elements.each do |umm_element, data|
      next unless umm_element.kind_of?(UmlMetamodel::Package)
      umm_element.imports.each do |umm_impt|
        ii = factory.createPackageImportInstance
        ii.setImportedPackage(elements[umm_impt][:md_element])
        ii.setImportingNamespace(data[:md_element])
      end
    end
  end
  
  def set_documentation
    elements.each do |umm_element, info|
      begin
        doc = umm_element.documentation if umm_element.documentation && !umm_element.documentation.strip.empty?
        if doc
          # if documentation starts and ends with angle brackets, wrap in html tag
          doc = HTMLHelper.wrap_by_html_body(doc) if doc =~ /\A<.*>\z/m
          ModelHelper.setComment(elements[umm_element][:md_element], doc)
        end
      # NOTE: adding exception information here. Previous rescue block completely hid actually error message. -SD
      rescue Exception => ex
        problem = "WARNING: Could not set documentation on #{umm_element.qualified_name} named #{umm_element.qualified_name} -> documentation: '#{umm_element.documentation}' (error was #{ex.message})"
        puts Rainbow(problem).yellow
        messages << problem
      end
    end
  end
  
  def apply_stereotypes
    elements.each do |umm_element, info|
      next unless umm_element.is_a?(UmlMetamodel::Element) # because UmlMetamodel::Project is not a type of Element
      md_element = elements[umm_element][:md_element]
      umm_element.applied_stereotypes.each do |applied_stereotype|
        umm_stereotype = applied_stereotype.instance_of
        unless umm_stereotype
          problem = "ERROR: Can not find umm_stereotype for applied stereotype on #{umm_element.qualified_name} -- #{umm_element.applied_stereotypes.inspect}"
          puts Rainbow(problem).yellow
          messages << problem
          next
        end
        md_stereotype  = elements[umm_stereotype][:md_element]
        begin
          md_element.apply_stereotype(md_stereotype)
        rescue
          problem = "ERROR: Problem applying stereotype #{md_stereotype.getQualifiedName} on #{md_element.getQualifiedName}"
          puts Rainbow(problem).yellow
          messages << problem
        end
        applied_stereotype.applied_tags.each do |umm_applied_tag|
          md_tag  = elements[umm_applied_tag.instance_of][:md_element]
          begin
            # Delete pre-existing value(s)
            # NOTE: this may have the effect of removing values from an applied stereotype even if no new value is added
            # TODO: move this logic to magicdraw_extensions
            StereotypesHelper.clearStereotypeProperty(md_element, md_stereotype, md_tag.name)
            # Apply new value
            md_element.apply_tag_with_value(md_stereotype, md_tag, umm_applied_tag.value)
          rescue 
            problem = "ERROR: Failed to apply stereotype tag #{md_tag.getQualifiedName} to #{md_element.getQualifiedName} with value '#{umm_applied_tag.value}'"
            puts Rainbow(problem).yellow
            messages << problem
          end
        end
      end
    end
  end
  
  def find_md_element_by_id(id)
    MagicDraw.project.getElementByID(id)
  end
  
  # you can have two elements with the same qualified name because we separated AssociationClass into Association + Class.  In practice we are/should not (be) giving the Association a name in this case.
  # you sometimes need the option to make sure you get the class instead of the association class
  def find_umm_element_by_name(str, reject_assocs = false)
    elements.keys.find { |el| el.qualified_name == str && (reject_assocs ? !el.is_a?(UmlMetamodel::Association) : true)}
  end
    
  # FIXME deal with conflicts arising from existing element with desired name (in desired namespace) but different ID.
  def make(umm_element)
    existing_md_element = elements[umm_element][:md_element]
    return existing_md_element if existing_md_element
    # puts Rainbow("Making #{umm_element.class} #{umm_element.name}").green
    element_class = umm_element.class
    case
    when element_class == UmlMetamodel::AppliedStereotype
      return
    when element_class == UmlMetamodel::AppliedTag
      return
    when element_class == UmlMetamodel::Association
      make_association(umm_element) unless umm_element.association_class
    when element_class == UmlMetamodel::Class
      if umm_element.association_class?
        make_association_class(umm_element)
      else
        make_class(umm_element)
      end
    when element_class == UmlMetamodel::Datatype
      make_datatype(umm_element)
    when element_class == UmlMetamodel::Enumeration
      make_enumeration(umm_element)
    when element_class == UmlMetamodel::EnumerationLiteral
      make_literal(umm_element)
    when element_class == UmlMetamodel::Interface
      make_interface(umm_element)
    when element_class == UmlMetamodel::Model
      make_model(umm_element)
    when element_class == UmlMetamodel::Profile
      make_profile(umm_element)
    when element_class == UmlMetamodel::Package
      make_package(umm_element)
    when element_class == UmlMetamodel::Primitive
      make_primitive(umm_element)
    when element_class == UmlMetamodel::Project
      return
    when element_class == UmlMetamodel::Property
      # If it is a Property that is a memberEnd of an association then it will be made later
      make_attribute(umm_element) unless umm_element.association
    when element_class == UmlMetamodel::Stereotype
      make_stereotype(umm_element)
    when element_class == UmlMetamodel::Tag
      make_tag(umm_element)
    else
      raise "I don't know how to make a #{umm_element.inspect}"
    end
  end
  
  def set_ownerships(umm_element, md_owner)
    # return if umm_element.is_a?(UmlMetamodel::Project)
    puts Rainbow(umm_element.qualified_name).red unless elements[umm_element]
    elements[umm_element][:md_owner] = md_owner
    md_element    = elements[umm_element][:md_element]
    element_class = umm_element.class
    # puts Rainbow("No md_element: #{umm_element.inspect}").red unless md_element
    case
    when element_class == UmlMetamodel::AppliedStereotype
      return
    when element_class == UmlMetamodel::AppliedTag
      return
    when element_class == UmlMetamodel::Association
      return if umm_element.association_class
      md_element.setOwner(md_owner)
    when [UmlMetamodel::Class, UmlMetamodel::Datatype, UmlMetamodel::Interface, UmlMetamodel::Primitive].include?(element_class)
      md_element.setOwningPackage(md_owner)
      umm_element.properties.each { |prop| set_ownerships(prop, md_element) }
    when element_class == UmlMetamodel::Enumeration
      md_element.setOwningPackage(md_owner)
      umm_element.literals.each { |lit| set_ownerships(lit, md_element) }
      umm_element.properties.each { |prop| set_ownerships(prop, md_element) unless prop.name == 'value' }
    when element_class == UmlMetamodel::EnumerationLiteral
      md_element.setOwner(md_owner)
    when [UmlMetamodel::Model, UmlMetamodel::Profile, UmlMetamodel::Package].include?(element_class)
      md_element.setOwningPackage(md_owner)
      umm_element.contents.each { |e| set_ownerships(e, md_element) }
    when element_class == UmlMetamodel::Project
      # puts Rainbow(md_owner.inspect).red
      umm_element.contents.each { |e| set_ownerships(e, md_owner) }
    when element_class == UmlMetamodel::Property
      md_element.setClassifier(md_owner) unless umm_element.association
    when element_class == UmlMetamodel::Stereotype
      md_element.setOwner(md_owner)
      umm_element.tags.each { |tag| set_ownerships(tag, md_element) }
    when element_class == UmlMetamodel::Tag
      md_element.setOwner(md_owner)
    else
      raise "I don't know how to make a #{element.inspect}"
    end
  end
  
  def set_id(umm_element, md_element)
    id_to_set = umm_element.id && umm_element.id.split(UmmAdapter::ID_CONCAT_CHAR).first
    found = find_md_element_by_id(id_to_set)
    if found
      puts Rainbow("ID for #{umm_element.qualified_name} already in MagicDraw -- #{id_to_set}").red
      puts caller[0..9];puts
    end
    counter     = MagicDraw.project.getCounter
    old_setting = counter.canResetIDForObject
    counter.setCanResetIDForObject(true)
    begin
      md_element.setID(id_to_set) if id_to_set
    rescue Exception => ex
      msg = "ERROR: Failed to set id '#{id_to_set}' on #{md_element} that correlates to #{umm_element.inspect}"
      puts Rainbow(msg).red
      messages << msg
      puts ex.message
    ensure
      counter.setCanResetIDForObject(old_setting)
    end
  end
  
  def factory; MagicDraw.factory; end
  
  # returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Package
  def make_package(umm_element)
    md_pkg = factory.createPackageInstance
    set_name_and_id(md_pkg, umm_element)
  end
  
  # returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Model
  def make_model(umm_element)
    md_pkg = factory.createModelInstance
    set_name_and_id(md_pkg, umm_element)
  end
  
  # returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Profile
  def make_profile(umm_element)
    md_pkg = factory.createProfileInstance
    set_name_and_id(md_pkg, umm_element)
  end
  
  def make_literal(umm_element)
    lit = factory.createEnumerationLiteralInstance
    set_name_and_id(lit, umm_element)
  end
  
  # Returns a com.nomagic.uml2.ext.magicdraw.classes.mdkernel.Class
  # (Specifically Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class)
  def make_class(umm_element)
    md_class = factory.createClassInstance
    configure_classifier(md_class, umm_element)
  end
  
  def make_association_class(umm_element)
    md_class = factory.createAssociationClassInstance  
    # I don't think assoc classes can be abstract in practice
    configure_classifier(md_class, umm_element)
  end

  def make_datatype(umm_element)
    md_datatype = factory.createDataTypeInstance  
    configure_classifier(md_datatype, umm_element)
  end
  
  # Returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::PrimitiveType 
  def make_primitive(umm_element)
    md_prim = factory.createPrimitiveTypeInstance
    configure_classifier(md_prim, umm_element)
  end
  
  # Returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Enumeration
  def make_enumeration(umm_element)
    md_enum = factory.createEnumerationInstance
    md_enum.setName(umm_element.name)
    set_id(umm_element, md_enum)
    md_enum
  end
  
  # Returns Java::ComNomagicUml2ExtMagicdrawClassesMdinterfaces::Interface
  def make_interface(umm_element)
    md_intf = factory.createInterfaceInstance
    configure_classifier(md_intf, umm_element)
  end
  
  def configure_classifier(md_element, umm_element)
    set_name_and_id(md_element, umm_element)
    md_element.setAbstract(umm_element.is_abstract)
    md_element
  end
  
  def set_name_and_id(md_element, umm_element)
    md_element.setName(umm_element.name)
    set_id(umm_element, md_element)
    md_element
  end
  
  def make_stereotype(umm_element)
    md_stereotype = factory.createStereotypeInstance
    set_name_and_id(md_stereotype, umm_element)
  end

  def make_tag(umm_element)
    md_tag = factory.createPropertyInstance
    set_name_and_id(md_tag, umm_element)
  end

  def configure_property(md_prop, umm_element)
    set_name_and_id(md_prop, umm_element)
    set_visibility(md_prop, umm_element)
    md_prop.setOrdered(umm_element.is_ordered) if umm_element.is_ordered
    if (agg_kind = umm_element.aggregation)
      case
      when agg_kind == :composite
        md_prop.setAggregation(MagicDraw::COMPOSITE)
      when agg_kind == :shared
        md_prop.setAggregation(MagicDraw::SHARED)
      end
    end
    md_prop.setDerived(true) if umm_element.is_derived
    md_prop.setUnique(true)  if umm_element.is_unique
    # Set multiplicity (except for 0..1 vs unspecified)
    current_multiplicity = ModelHelper.getMultiplicity(md_prop)
    new_multiplicity = umm_element.multiplicity_string
    if current_multiplicity.empty? && new_multiplicity == '0..1'
      # Do nothing, since empty string is treated as equivalent to 0..1
    else
      ModelHelper.setMultiplicity(new_multiplicity, md_prop)
    end
    md_prop
  end
  
  def make_attribute(umm_element)
    md_prop = factory.createPropertyInstance
    configure_property(md_prop, umm_element)
    ModelHelper.setNavigable(md_prop, !!umm_element.is_navigable)
    set_default_value(umm_element, md_prop)
    md_prop
  end
  
  # TODO There are some primitives types here that will fall through to String.  Is that OK?
  def set_default_value(umm_property, md_property)
    dv = umm_property.default_value
    return unless dv
    type_name = umm_property.type && umm_property.type.name
    case type_name
    when "Integer"
      md_dv = factory.createLiteralIntegerInstance
    when "UnlimitedNatural"
      md_dv = factory.createLiteralUnlimitedNaturalInstance
    when "Real", "Float"
      md_dv = factory.createLiteralRealInstance
    when "Boolean"
      md_dv = factory.createLiteralBooleanInstance
    when "Null"
      md_dv = factory.createLiteralNullInstance
    else # this should also catch untyped properties
      md_dv = factory.createLiteralStringInstance
    end
    md_dv.setValue(dv)
    md_property.setDefaultValue(md_dv)
  end

  # Returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Association
  def make_association(umm_element)
      md_assoc = factory.createAssociationInstance
      set_name_and_id(md_assoc, umm_element)
  end
    
  # Returns Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Generalization
  # Skips if generalization already exists in MD model
  def generalize(child, parent)
    return nil if parent==child # return value is not currently used - bailing out now avoids: Invalid inheritance: Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class Entity can not inherit from Java::ComNomagicUml2ExtMagicdrawClassesMdkernel::Class Entity
    raise "Invalid inheritance: #{child.class.name} #{child.getName} can not inherit from #{parent.class.name} #{parent.getName}" unless ModelHelper.isLegalInheritance(parent, child)
    puts "Warning: Potentially invalid inheritance: #{child.class.name} #{child.getName} and #{parent.class.name} #{parent.getName} are different metaclasses" unless child.class == parent.class
    # Check for pre-existing inheritance from parent
    return nil if child.getGeneral.any?{|p| parent == p}
    gen = factory.createGeneralizationInstance
    ModelHelper.setClientElement(gen, child)
    ModelHelper.setSupplierElement(gen, parent)
    gen
  end
  
  # Returns an InterfaceRealization
  def realize(owning_package, implementing_class, interface)
    # puts "Class is instance of #{implementing_class.class.name}, named #{implementing_class.name} Interface is instance of #{interface.class.name}, named #{interface.name}"
    # If the following test failes due to an invalid argument, please have a look at the comment on $USE_MD_ENUMERATIONS
    possible = ModelHelper.isLegalInterfaceRealization(interface, implementing_class) 
    raise "Invalid realization: #{implementing_class.getName} can not implement #{interface.getName}" unless  possible
    rel = factory.createInterfaceRealizationInstance # createRealizationInstance
    rel.setOwningPackage(owning_package) # Unlike Generalization, Realization needs to have owning package set (have not verified this is the case for InterfaceRealization)
    ModelHelper.setSupplierElement(rel, interface)
    ModelHelper.setClientElement(rel, implementing_class)
    # rel.setOwner(implementing_class) # Does not appear to be necessary
    rel
  end
  
end
