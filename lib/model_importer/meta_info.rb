# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module ModelImporter
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  GEM_NAME         = "plugin.model_importer"
  VERSION          = '0.1.0'
  AUTHORS          = ["Michael Faughn"]
  EMAILS           = ["mf@prometheuscomputing.com"]
  HOMEPAGE         = nil
  SUMMARY          = %q{Imports instances of the UML metamodel expressed in the Prometheus Computing Ruby format}
  DESCRIPTION      = %q{Imports instances of the UML metamodel expressed in the Prometheus Computing Ruby format, into MagicDraw.}
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.3']
  RUNTIME_VERSIONS = {
    :jruby => ['>= 9.1']
  }
  APP_TYPES = []
  LAUNCHER  = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # If JRuby platform Ruby code depends on a third party Java jar, that goes in DEPENDENCIES_JAVA
  DEPENDENCIES_RUBY  = {
    :magicdraw_extensions => '~> 0.13',
    :uml_metamodel        => '~> 3.0',
    :umm_adapter          => '~> 0.0',
    :rainbow              => '~> 3.0'
  }
  DEPENDENCIES_MRI   = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY  = { } 
  DEVELOPMENT_DEPENDENCIES_MRI   = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { :magicdraw_plugin_runner => '~> 0.0' } # Goes in the MagicDraw gemset!
  
  DEPENDENCIES_JAVA    = { }
  DEPENDENCIES_JAVA_SE = { }
  DEPENDENCIES_JAVA_ME = { }
  DEPENDENCIES_JAVA_EE = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
  
end