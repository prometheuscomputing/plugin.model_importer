require 'model_importer/importer.rb'
case $Action
when :import_models
  UmmImporter.import_models($SELECTION)
else
  raise "Unexpected action: #{$Action.inspect}"
end
